// const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

/*************Different methods*************/
//app.get()
// app.post()
// app.put()
// app.delete()

// /*********Requesting to base url '/'*********/
// app.get('/', (req,res)=>
// {
//     res.send("Hello aBnOrMaL ePiC")
// })

// /************call to /api/courses************/
// app.get('/api/courses', (req, res)=>
// {
//     res.send([1,2,3])
// })

// /************call to /api/courses/id************/
// app.get('/api/courses/:id', (req, res)=>
// {
//     res.send(req.params.id)
// })

// /************call to /api/courses/id query params************/
// app.get('/api/courses/:id/q', (req, res)=>
// {
//     res.send(req.query)
// })
// /*******************/

const courses = 
[
    { id: 1, name: 'course1' },
    { id: 2, name: 'course2' },
    { id: 3, name: 'course3' },
]

/************call to /api/courses************/
app.get('/api/courses', (req, res)=>
{
    res.send(courses)
})

/************call to /api/courses/id query params************/
app.get('/api/courses/:id', (req, res)=>
{
    const course = courses.find(coursesItem => coursesItem.id === parseInt(req.params.id))

    if(!course) //404
        res.status(404).send('Course with the id = '+req.params.id+' not found')
    res.send(course)
})
/************call to /api/courses/id query params POST************/
app.post('/api/courses', (req, res)=>
{
    // JOI
    // const schema =
    // {
    //     name: Joi.string().min(3).required
    // }

    // const result = Joi.validate(req.body, schema)
    // console.log(result)
    //Basic validation
    if(!req.body.name || req.body.name.length < 3)
    {
        //400 Bad Req
        res.status(400).send("Name should be at least 3 characters long")
        return;
    }
    const course =
    {
        id: courses.length + 1,
        name: req.body.name
    }
    courses.push(course);
    res.send(course);
})

/***************Put request***************/
app.put("/api/courses/:id", (req, res)=>
{
    //Look up to course id
    const course = courses.find(c => c.id === parseInt(req.params.id))
    
    //If not existing, return 404
    if (!course)
        res.status(404).send("404, requested object not found")

    //validate
    if(!req.body.name || req.body.name.length < 3)
    {
        //400 Bad Req
        res.status(400).send("Name should be at least 3 characters long")
        return;
    }

    //update course
    course.name = req.body.name

    //return the updated course in the send function
    res.send(course);
});

//https://youtu.be/pKd0Rpw7O48?t=3217

//Listening to ports
const port = process.env.PORT || 3000
app.listen(port,()=> console.log(`Listening on port ${port}`) )